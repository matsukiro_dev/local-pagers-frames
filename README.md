#### В файле ```index.html``` подключен один iframe (есть проблемы с доступом из одного iframe к элементам другого iframe)
#### ```iframe >> iframe``` (основной iframe должен "лежать" вне тега ```body```)

В файле ```navigations.html``` основной скрипт на "чистом" javascript (без использования ```jQuery``` и ```ajax```).
Поэтому все данные по структуре разделов хранятся в переменных ```groups```, ```subgroups```, ```items``` **([строка 27](https://bitbucket.org/matsukiro_dev/local-pagers-frames/src/25360a7a056fcf719b5df347e8fe36af40699586/navigations.html#lines-27))**

# СТРУКТУРА ДАННЫХ
## groups
Группа первого уровня. Обязательные ключи: ```title```, ```id```
## subgroups
Группа второго уровня. Обязательные ключи: ```title```, ```id```, ```groupId``` (последний указывает, к какой группе первого уровня относится данная группа второго уровня)
## items
Группа третьего уровня. Обязательные ключи: ```title```, ```template```, ```subGroupId``` (последний указывает, к какой группе второго уровня относится данная группа третьего уровня)
